package co.thecomet.placeholderregistrar.placeholders;

import be.maximvdw.animatednames.api.PlaceholderAPI;
import org.bukkit.entity.Player;

import java.util.function.Function;

public class AnimatedNamesPlaceholderManager implements IPlaceholderManager {
    @Override
    public void register(String placeholder, Function<Player, String> function) {
        PlaceholderAPI.registerPlaceholder(placeholder, event -> function.apply(event.getPlayer()));
    }
}
