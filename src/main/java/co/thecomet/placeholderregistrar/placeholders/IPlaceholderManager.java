package co.thecomet.placeholderregistrar.placeholders;

import org.bukkit.entity.Player;

import java.util.function.Function;

public interface IPlaceholderManager {
    public void register(String placeholder, Function<Player, String> function);
}
