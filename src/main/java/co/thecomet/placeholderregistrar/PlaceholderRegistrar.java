package co.thecomet.placeholderregistrar;

import co.thecomet.placeholderregistrar.placeholders.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class PlaceholderRegistrar extends JavaPlugin {
    private static List<IPlaceholderManager> managers = new ArrayList<>();
    
    public void onEnable() {
        init();
    }
    
    private void init() {
        if (Bukkit.getPluginManager().isPluginEnabled("ActionBar")) {
            this.getLogger().info("ActionBar Manager Registered!");
            managers.add(new ActionBarPlaceholderManager());
        }

        if (Bukkit.getPluginManager().isPluginEnabled("AnimatedNames")) {
            this.getLogger().info("AnimatedNames Manager Registered!");
            managers.add(new AnimatedNamesPlaceholderManager());
        }

        if (Bukkit.getPluginManager().isPluginEnabled("DynamicSigns")) {
            this.getLogger().info("DynamicSigns Manager Registered!");
            managers.add(new DynamicSignsPlaceholderManager());
        }

        if (Bukkit.getPluginManager().isPluginEnabled("FeatherBoard")) {
            this.getLogger().info("FeatherBoard Manager Registered!");
            managers.add(new FeatherBoardPlaceholderManager());
        }

        if (Bukkit.getPluginManager().isPluginEnabled("Tab")) {
            this.getLogger().info("Tab Manager Registered!");
            managers.add(new TabPlaceholderManager());
        }

        if (Bukkit.getPluginManager().isPluginEnabled("TitleMotdAdvanced")) {
            this.getLogger().info("TitleMotdAdvanced Manager Registered!");
            managers.add(new TitleMotdAdvancedPlaceholderManager());
        }
    }

    public static void register(String placeholder, Function<Player, String> function) {
        for (IPlaceholderManager manager : managers) {
            manager.register(placeholder, function);
        }
    }
}
